function isValid(inp) {
  if (isNaN(inp) || inp.indexOf(".") > -1 || !isFinite(inp)) {
    return false;
  } else {
    return true;
  }
}

function main() {
  const firstInp = window.prompt("Введите первое число");
  if (!isValid(firstInp)) {
    return "Некорректный ввод!";
  }
  const secondInp = window.prompt("Введите первое число");
  if (!isValid(secondInp)) {
    return "Некорректный ввод!";
  }
  const num1 = parseInt(firstInp, 10);
  const num2 = parseInt(secondInp, 10);
  return `Ответ: ${num1 + num2}, ${num1 / num2}.`;
}

console.log(main());
