const firstInp = window.prompt("Введите первое число");
const secondInp = window.prompt("Введите второе число");

function isValid(inp) {
  if (isNaN(inp) || inp.indexOf(".") > -1 || !isFinite(inp)) {
    return false;
  } else {
    return true;
  }
}

function powPrompt(firstInp, secondInp) {
  if (isValid(firstInp) && isValid(secondInp) && 1 < parseInt(secondInp, 10) < 37) {
    return parseInt(firstInp, 10).toString(parseInt(secondInp, 10));
  } else {
    return "Некорректный ввод!";
  }
}
console.log(powPrompt(firstInp, secondInp));
