function checkNumber(val) {
    if (typeof val === "number" && isFinite(val) && !isNaN(val)) {
      return true;
    } else {
      return false;
    }
  }
  
  function checkArray(obj) {
    if (obj instanceof Array) {
      return true;
    } else {
      return false;
    }
  }
  
  function checkNumberArray(arr) {
    const res = arr.every((el) => checkNumber(el));
    if (res) {
      return res;
    } else {
      return false;
    }
  }
  
  function selectFromInterval(numArr, num1, num2) {
    if (!checkArray(numArr)) {
      throw new Error("Argument isn't Array");
    }
    if (!checkNumberArray(numArr)) {
      throw new Error("Array contains NaN values");
    }
    if (!checkNumber(num1) || !checkNumber(num2)) {
      throw new Error("Interval values aren't Numbers");
    }
    let result = [];
    if (num1 > num2) {
      result = numArr.filter((el) => el <= num1 && el >= num2);
    } else {
      result = numArr.filter((el) => el >= num1 && el <= num2);
    }
    return result;
  }
  
  //tests
  
  // console.log(selectFromInterval([1, 3, 5], 5, 2));
  // console.log(selectFromInterval([1, 3, 5], 3, 3));
  // console.log(selectFromInterval([-2, -15, 0, 4], -13, -5));
  // console.log(selectFromInterval([1, 2, 3, 5, 6], 0.5, 5));
  // console.log(selectFromInterval([-0.1, 0.2, 1.3], 0, 3));
  //   console.log(selectFromInterval([2, 3, 5], "2", "5"));
  //   console.log(selectFromInterval({}, "2", "5"));
  //   console.log(selectFromInterval(["aaaa"], 2, 3));
  